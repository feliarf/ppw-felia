from django.shortcuts import render
from .models import schedule
from .forms import jadwal
from django.http import HttpResponseRedirect
response = {'author' : 'Felia Risky Faizal'}
# Create your views here.
def index(request):
	return render(request,'Home.html')
def profil(request):
	return render(request,'webb.html')
def kemampuan(request):
	return render(request,'Kemampuan.html')
def kontak(request):
	return render(request,'Kontak.html')
def form(request):
	return render(request,'Form2.html')


def schedule_input(request):
	form = jadwal(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['nama_kegiatan'] = request.POST['nama_kegiatan']
		response['hari'] = request.POST['hari']
		response['tanggal'] = request.POST['tanggal']
		response['tempat'] = request.POST['tempat']
		response['kategori'] = request.POST['kategori']
		response['jam'] = request.POST['jam']
		jadwal_kegiatan = schedule(nama_kegiatan=response['nama_kegiatan'], hari=response['hari'], tanggal=response['tanggal'], tempat=response['tempat'], kategori=response['kategori'], jam=response['jam'])
		jadwal_kegiatan.save()
		return HttpResponseRedirect('../scheduleInput')
	else:
		print('not found')
	response['jadwal'] = form
	html = 'schedule.html'
	return render(request, html, response)
		

def schedule_call(request):
	schedules = schedule.objects.all()
	response = {'jadwal' : schedules}
	return render(request, 'result.html', response)
	
def schedule_delete(request):
	schedule.objects.all().delete()
	return HttpResponseRedirect('../scheduleCall')