from django.conf.urls import url
from .views import index
from .views import profil
from .views import kemampuan
from .views import kontak
from .views import form
from .views import schedule_input
from .views import schedule_call
from .views import schedule_delete

urlpatterns = [
	url(r'^$', index, name ='index'),
	url(r'^profil/', profil, name ='profil'),
	url(r'^kemampuan/', kemampuan, name ='kemampuan'),
	url(r'^kontak/', kontak, name ='kontak'),
	url(r'^form/', form, name ='form'),
	url(r'^scheduleInput/', schedule_input, name = 'schedule_input'),
	url(r'^scheduleCall/', schedule_call, name = 'schedule_call'),
	url(r'^scheduleDelete/', schedule_delete, name = 'schedule_delete'),
]