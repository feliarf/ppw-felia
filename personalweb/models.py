from django.db import models
from django.utils import timezone
from datetime import datetime

class schedule(models.Model):
	nama_kegiatan = models.CharField(max_length = 50)
	hari = models.CharField(max_length = 10)
	tanggal = models.DateField()
	jam = models.TimeField()
	tempat = models.CharField(max_length = 20)
	kategori = models.CharField(max_length = 20)

# Create your models here.
