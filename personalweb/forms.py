from django import forms
class jadwal(forms.Form):

	nama_kegiatan = forms.CharField(label='Nama Kegiatan', required = True, max_length = 50, widget = forms.TextInput(attrs={'class' : 'form-control'}))
	hari = forms.CharField(label='Hari', required = True, max_length = 10, widget = forms.TextInput(attrs={'size':50, 'class' : 'form-control'}))
	tanggal = forms.DateField(label='Tanggal', required = True, widget = forms.TextInput(attrs={'type' : 'date', 'class' : 'form-control'}) )
	jam = forms.TimeField(label='Jam', required = True, widget = forms.TextInput(attrs={'type' : 'time', 'class' : 'form-control'}))
	tempat = forms.CharField(label='Tempat', required = True, max_length = 20, widget = forms.TextInput(attrs={'size':80, 'class' : 'form-control'}))
	kategori = forms.CharField(label='Kategori', required = True, max_length = 20, widget = forms.TextInput(attrs={'size': '80' , 'class' : 'form-control'}))